﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Service2.App.Services;
using Service2.Shared;
using System;

namespace Service1.UnitTest.Tests
{
    [TestClass]
    public class CalcTest
    {
        private ContractApp contractApp;

        public CalcTest()
        {
            contractApp = Substitute.For<ContractApp>();
        }


        /* Necessário rodar a API 1 para executar esse teste */
        //[TestMethod]
        //public void GetComposeTaxes()
        //{
        //    string valorInicial = "100";
        //    string meses = "5";
        //    var response = contractApp.GetComposeTaxes(valorInicial, meses);

        //    Assert.AreEqual(response.Data.Value, Convert.ToDecimal(105.1));
        //    Assert.AreEqual(response.Data.FormattedValue, "105,10");
        //}

        [TestMethod]
        public void GetInfo()
        {
            var response = contractApp.GetInfos();

            Assert.AreEqual(response.Data.ServiceOne, Constants.PROJECT_ONE_SOURCE);
            Assert.AreEqual(response.Data.ServiceTwo, Constants.PROJECT_TWO_SOURCE);
        }
    }
}
