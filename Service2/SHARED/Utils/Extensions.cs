﻿using System;

namespace Service2.Shared.Utils
{
    public static class Extensions
    {
        public static string FormattedTruncate(this double value)
        {
            return value.ToString("0.00");
        }

        public static decimal ToDecimal(this string value)
        {
            return Convert.ToDecimal(value);
        }
    }
}
