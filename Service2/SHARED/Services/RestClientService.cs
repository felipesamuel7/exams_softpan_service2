﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Service2.Shared.Services
{
    public static class RestClientService
    {
        private static HttpClient httpClient;

        static RestClientService()
        {
            var handler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = delegate { return true; }
            };

            httpClient = new HttpClient(handler);
            httpClient.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));
        }

        public async static Task<string> GetAsync(string url)
        {
            try
            {
                using (HttpRequestMessage message = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(url)
                })
                {
                    using (var httpResponse = httpClient.SendAsync(message))
                    {
                        HttpResponseMessage response;

                        try
                        {
                            response = await httpResponse;
                        }
                        catch (Exception ex)
                        {
                            ex.Data.Add("url", url);
                            throw ex;
                        }

                        if (response.IsSuccessStatusCode)
                        {
                            return await httpResponse.Result.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            throw new Exception($"{ response.ReasonPhrase }{ await response.Content.ReadAsStringAsync() }{ (int)response.StatusCode }{ response.Headers }");
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
