﻿namespace Service2.Domain.Entities
{
    public class Info
    {
        public string ServiceOne { get; set; }
        public string ServiceTwo { get; set; }
    }
}
