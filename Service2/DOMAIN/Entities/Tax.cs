﻿namespace Service2.Domain.Entities
{
    public class Tax
    {
        public decimal Value { get; set; }
        public string FormattedValue { get; set; }
    }
}
