﻿using Newtonsoft.Json;
using Service2.App.ViewModels;
using Service2.Domain.Entities;
using Service2.Shared;
using Service2.Shared.Services;
using Service2.Shared.Utils;
using System;

namespace Service2.App.Services
{
    public class ContractApp
    {
        public ResponseViewModel<Tax> GetComposeTaxes(string valorinicial, string meses)
        {
            ResponseViewModel<Tax> response = new ResponseViewModel<Tax>();
            try
            {
                if (double.TryParse(valorinicial, out double v) && double.TryParse(meses, out double m))
                {
                    string responseTax = RestClientService.GetAsync(Constants.API1_ENDPOINT).Result;
                    decimal? tax = JsonConvert.DeserializeObject<ResponseViewModel<Tax>>(responseTax)?.Data?.Value;

                    if (tax.HasValue)
                    {
                        response.Data = new Tax
                        {
                            FormattedValue = (v * Math.Pow((1 + Convert.ToDouble(tax.Value)), m)).FormattedTruncate()
                        };
                        response.Data.Value = response.Data.FormattedValue.ToDecimal();
                        
                        response.Error = false;
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                response.Error = true;
                response.Message = ex.Message;

                return response;
            }
        }

        public ResponseViewModel<Info> GetInfos()
        {
            ResponseViewModel<Info> response = new ResponseViewModel<Info>();
            try
            {

                response.Data = new Info
                {
                    ServiceOne = Constants.PROJECT_ONE_SOURCE,
                    ServiceTwo = Constants.PROJECT_TWO_SOURCE
                };
                response.Error = false;

                return response;
            }
            catch (Exception ex)
            {
                response.Error = true;
                response.Message = ex.Message;

                return response;
            }
        }
    }
}
