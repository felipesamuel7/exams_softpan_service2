﻿using Microsoft.AspNetCore.Mvc;
using Service2.App.Services;
using System;

namespace Service2.API.Controllers
{
    public class InfoController : Controller
    {
        /// <summary>
        /// Retorna os fontes dos projetos
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        [Route("/showmethecode")]
        [HttpGet]
        public IActionResult Get([FromServices] ContractApp app)
        {
            try
            {
                return Ok(app.GetInfos());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}