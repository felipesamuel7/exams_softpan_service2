﻿using System;
using Microsoft.AspNetCore.Mvc;
using Service2.App.Services;

namespace Service2.API.Controllers
{
    public class CalcController : ControllerBase
    {
        /// <summary>
        /// Calcula juros compostos
        /// </summary>
        /// <param name="valorinicial"></param>
        /// <param name="meses"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        [Route("/calculajuros")]
        [HttpGet]
        public IActionResult Get([FromQuery] string valorinicial, [FromQuery] string meses, [FromServices] ContractApp app)
        {
            try
            {
                return Ok(app.GetComposeTaxes(valorinicial, meses));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
